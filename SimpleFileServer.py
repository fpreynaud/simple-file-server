#!/usr/bin/python

import socket
import argparse
import threading
import os.path
import re

parser = argparse.ArgumentParser()
parser.add_argument('--bindto', type=str, default='127.0.0.1')
parser.add_argument('--port', type=int, default=4444)
args = parser.parse_args()
pos  = 0
uploadFolder = '/root/upload/'
EXIT = 0
UPLOAD = 1

def read_command(cl_socket):
	command = cl_socket.recv(1024)
	if re.match(r'^(?i)GET\s', command):
		filename = ' '.join(command.split(command[3])[1:]).rstrip()
		print(filename + ' requested')
		upload(cl_socket, filename)
		return UPLOAD
	if re.match(r'^(?i)QUIT\s*$', command):
		cl_socket.close()
		return EXIT

def upload(cl_socket, filename):
	if os.path.exists(os.path.join(uploadFolder,filename)):
		with open(os.path.join(uploadFolder,filename), 'rb') as f:
			print('Sending ' + f.name)
			try:
				buf = f.read(1024)
				pos = f.tell()

				while buf != '':
					cl_socket.send(buf)
					buf = f.read(1024)
					pos = f.tell()
				print('Transfer complete')
			except KeyboardInterrupt:
				exit(1)
			except Exception as e:
				print(e)
				f.seek(pos)
	else:
		cl_socket.send('"{0}" does not exist\n'.format(filename))
	
def session(cl_socket):
	while read_command(cl_socket) != EXIT: 
		pass

def listen():
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((args.bindto, args.port))
	s.listen(5)
	print('Waiting for connections')

	while True:
		(cl_socket, addr) = s.accept()
		print('Connection from {0}'.format(addr))
		threading.Thread(target=session,name=':'.join(map(str,addr)),args=(cl_socket,)).start()
		
listen()
